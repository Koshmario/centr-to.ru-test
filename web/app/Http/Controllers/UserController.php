<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;

class UserController extends Controller
{
    public function allAuthors()
    {
        return view('authors', ['users' => User::all()]);
    }

    public function profile(int $id)
    {
        $user = User::findOrFail($id);
        return view('profile', ['user' => $user]);
    }

    public function edit(int $id)
    {
        $user = User::findOrFail($id);
        return view('edit', ['user' => $user]);
    }

    private function validation(Request $request, $validators)
    {
        $validator = Validator::make($request->all(), $validators);
        return $validator;
    }

    private function processData(Request $request)
    {
        $request->merge(['agree' => $request->input('agree') ?? 0]);

        if ($request->hasFile('update_img')) {
            $img = $request->update_img;
            $imgName = time() . '.' . $img->extension();
            $img->storeAs('public/img', $imgName);
            $request->merge(['profile_img' => '/storage/img/' . $imgName]);
        }

        return $request;
    }

    public function save(Request $request)
    {
        $validators = [
            'nickname' => 'required|string',
            'name' => 'required|string',
            'update_img' => 'mimes:jpg,bmp,png,svg,jpeg',
            'surname' => 'required|string',
            'phone' => 'required|string',
            'email' => 'required|string',
            'sex' => 'required|string',
            'agree' => 'nullable|string'
        ];

        $valid = self::validation($request, $validators);

        if ($valid->fails()) {
            return redirect()->route('author_create')
                ->withErrors($valid)
                ->withInput();
        }

        $requestData = self::processData($request);
        $user = User::create($requestData->all());

        return redirect()->route('author_profile', ['id' => $user->id]);
    }

    public function update(Request $request, int $id)
    {
        $validators = [
            'nickname' => 'string',
            'name' => 'string',
            'update_img' => 'mimes:jpg,bmp,png,svg,jpeg',
            'surname' => 'string',
            'phone' => 'string',
            'email' => 'string',
            'sex' => 'string',
            'agree' => 'nullable|string'
        ];

        $valid = self::validation($request, $validators);

        if ($valid->fails()) {
            return redirect()->route('author_edit', ['id' => $id])
                ->withErrors($valid)
                ->withInput();
        }

        $requestData = self::processData($request);
        $user = User::findOrFail($id);
        $user->update($requestData->all());

        return redirect()->route('author_profile', ['id' => $id]);
    }
}
