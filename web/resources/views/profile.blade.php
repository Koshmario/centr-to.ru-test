@extends('layouts.app')

@section('title', 'Add author')

@php
    /**
    * @var \App\Models\User $user
    */
@endphp

@section('content')
    <main class="row mb-2">
        <div class="col-12">

            <h2 class="heading mb-3">Author profile</h2>

            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="col-4">
                        <img src="{{ $user->profile_img }}" class="card-img" alt="...">
                    </div>
                    <div class="col-8">
                        <div class="card-body">
                            <p class="card-text"><span class="card-title h5">Name:</span> {{ $user->name }} {{ $user->surname }}</p>
                            <p class="card-text"><span class="card-title h5">Nickname:</span> {{ $user->nickname }}</p>
                            <p class="card-text"><span class="card-title h5">E-mail:</span> {{ $user->email }}</p>
                            <p class="card-text"><span class="card-title h5">Phone:</span> {{ $user->phone }}</p>
                            <a href="{{ route('author_edit', ['id'=> $user->id]) }}" class="btn btn-primary btn-lg">Edit profile</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </main>

    <div class="row">
        <div class="col-12">
            <h2 class="heading mb-3">Author articles</h2>
        </div>
    </div>

    @include('articles', ['articles' => $user->articles])
@endsection



