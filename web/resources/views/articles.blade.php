<div class="row">
    @foreach ($articles as $article)

        <div class="col-lg-3">
            <div class="card mb-4">

                <div class="card-body">
                    <h5 class="card-title">{{ $article->title }}</h5>

                    <ul class="list-inline text-sm">
                        @foreach($article->users as $user)

                            <li class="list-inline-item">
                                <a href="{{ url('authors') . '/' . $user->id }}" class="card-link">
                                    {{ $user->name }}
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <h6 class="card-subtitle mb-2 text-muted">{{ $article->text }}</h6>
                </div>

            </div>
        </div>
    @endforeach

</div>


