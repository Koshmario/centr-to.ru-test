@extends('layouts.app')

@section('title', 'Edit author')

@php
    /**
    * @var \App\Models\User $user
    */
@endphp

@section('content')

    <div class="row">
        <div class="col-lg-12 mb-5">
            <a href="{{ route('author_create') }}">
                <button type="button" class="btn btn-primary btn-lg">Add author</button>
            </a>
        </div>
    </div>

    <div class="row">
        @foreach ($users as $user)

            <div class="col-lg-3">
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="card-title">
                            <a href="{{ route('author_profile', ['id'=> $user->id]) }}"
                               class="card-link">{{ $user->name }} {{ $user->surname }}</a>
                        </h5>
                        <h6 class="card-subtitle mb-3 text-muted">{{ $user->nickname }}</h6>
                        <h6 class="card-subtitle mb-3 text-muted">{{ $user->email }}</h6>
                        <h6 class="card-subtitle text-muted">{{ $user->phone }}</h6>
                    </div>
                </div>
            </div>
        @endforeach

    </div>

@endsection
