<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'allAuthors']);
Route::get('/authors/{id}', [UserController::class, 'profile'])->name('author_profile');
Route::get('/authors/{id}/edit', [UserController::class, 'edit'])->name('author_edit');
Route::put('/authors/{id}/update', [UserController::class, 'update']);
Route::get('/author/create', function () { return view('create'); })->name('author_create');
Route::post('/author/save', [UserController::class, 'save']);
