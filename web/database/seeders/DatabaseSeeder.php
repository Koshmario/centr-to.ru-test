<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Article;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $articles = Article::factory(50)->create();
        User::factory(10)->create()->each(function (User $user) use ($articles) {
            $user->articles()->attach(
                $articles->random(rand(3, 10))->pluck('id')->toArray()
            );
        });
    }
}
