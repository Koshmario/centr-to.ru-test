#!/bin/bash
cp ./web/.env.example ./web/.env
docker-compose up -d --build
docker-compose exec php bash -c 'cd /web && composer install && php artisan key:generate && php artisan migrate --seed && php artisan storage:link && npm i && npm run dev'