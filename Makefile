build:
	docker-compose down
	docker-compose up -d --build
front-dev:
	docker-compose exec php bash -c "cd /web && npm run dev"