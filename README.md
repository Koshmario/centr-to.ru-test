## Деплой и запуск приложения
#### Небходимые технологии
- git
- docker (для linux) или docker desktop (для windows)
- docker-compose (дополнительно для linux)

#### Перечень действий и команд
1. Клонируем или скачиваем проект c репозитория
2. Открываем корневую папку приложения в консоли 
3. Есть возможность запустить скрипт start.sh, тогда приложение развернется автоматичекски, после чего необходимо продолжить чтение README.md с раздела "Структура приложения". Либо разворачиваем приложение в ручную. Для этого делаем следующие шаги: 
4. Заходим в папку web
5. Копируем файл .env.example в .env в эту же папку
6. Запускаем сборку контейнеров

    ``docker-compose up -d --build``
    
7. После сборки и запуска контейнеров заходим в консоль докера
    
    ``docker-compose exec php bash``
    
8. В консоли докера заходим в папку с проектом
    
    ``cd /web``
    
9. Устанавливаем composer
    
    ``composer install``
    
10. Генерируем ключ приложения

    ``php artisan key:generate``
    
11. Запускаем миграции с генерацией демо данных
    
    ``php artisan migrate --seed``
   
12. Далее прокидываем ссылки к хранилишу файлов, куда сохраняются загруженные аватарки
    
    ``php artisan storage:link``
   
13. И в завершение генерируем front приложения
    
    ``npm i && npm run dev``

## Структура приложения
Проходим по ссылке localhost в браузере, после чего откроется приложение на странице.

На главной странице выводиться кнопка для добавления автора и список уже существующих авторов. 

При нажатии на кнопку "Добавить автора" открываеться форма добавления автора. Все поля обязательны к заполненияю кроме загрузки аватарки и подписки на новости. Если другие поля мы не заполняем, то при нажатии на кнопку "Создать" срабатывает валидация и выводит ошибки на странице. При заполнении необходимых полей и нажатии кнопки "Создать" в базе создаеться новый автор и происходит редирект на его профиль. В профиле выводятся данные автора и его статьи. К новому автору статьи не привязываются, поэтому список статей будет пуст. Профиль можно редактировать при нажатии кнопки "Редактировать" 

Далее мы можем нажать на ссылку "Домой" и вернуться к списку авторов. Наш новый автор будет в конце списка.

Заходим к любому из сгенерированных авторов по клику в плашке по его имени. Откроется профиль автора. В профиле так же будут данные автора, кнопка редактирования профиля и список статей, принадлежащих автору. В плашках со статьями под заголовком выводяться ссылки на текущего и других авторов статьи. По клику мы можем перейти в профиль другого автора статьи. В профиле любого из авторов есть кнопка редактирования. При нажатии на кнопку открывается форма, заполненная данными из профиля. При замене значений и нажатии на кнопку "Обновить" происходит обновление записи в базе и редирект на отредактированный профиль.

Вот и все, что нужно знать о моем приложении :) Приятного тестирования!

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
